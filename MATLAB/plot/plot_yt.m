function figureSpectrogram = plot_yt(data,actionData)
%% Plot parameters
plot_fontsize = 16;
plot_fontname = 'Arial';
plot_fontweight = 'Bold';
plot_linewidth = 2;

if actionData.freqStart < 10^6
    FrequentyDivider = 10^3; %Hz to kHz
    yt_freq_Display = actionData.freqStart  / FrequentyDivider;
    FrequentyUnit = 'kHz';
elseif actionData.freqStart < 10^9
    FrequentyDivider = 10^6; %Hz to MHz
    yt_freq_Display = actionData.freqStart  / FrequentyDivider;
    FrequentyUnit = 'MHz';
else
    FrequentyDivider = 10^9; %Hz to GHz
    yt_freq_Display = actionData.freqStart  / FrequentyDivider;
    FrequentyUnit = 'GHz';
end

%check if a statistic is required

switch actionData.statistic
    case "median"
        data.Measurements = movmedian(data.Measurements,actionData.statisticTime);
        statsTexts ="Moving median over " + num2str(actionData.statisticTime) + " samples";
    case "min"
        data.Measurements = movmin(data.Measurements,actionData.statisticTime);
        statsTexts ="Moving min over " + num2str(actionData.statisticTime) + " samples";
    case "max"
        data.Measurements = movmax(data.Measurements,actionData.statisticTime);
        statsTexts ="Moving max over " + num2str(actionData.statisticTime) + " samples";
    case "std"
        data.Measurements = movstd(data.Measurements,actionData.statisticTime);
        statsTexts ="Moving std over " + num2str(actionData.statisticTime) + " samples";
    otherwise
        statsTexts ="Raw data";
end

%create X labels
X = data.MeasurementTimes;

%create new figure
figureSpectrogram = figure('outerposition',[0 0 1800 1200]);
plot(X,data.Measurements,'LineWidth',plot_linewidth);

%set the backupground color to gray for missing data
set(gca,'color',0.9*[1 1 1]);

%set labels
%set(gca, 'XTick',xticks, 'XTickLabel', xlabels);


if length(data.Date) > 1
    title([string(yt_freq_Display) + FrequentyUnit,"Date: " + string(data.Date(1)) + ' - ' + string(data.Date(end))+ " Location: " + data.LocationName], statsTexts)
else
    title([string(yt_freq_Display) + FrequentyUnit,"Date: " + string(data.Date(1)) + " Location: " + data.LocationName],statsTexts)
end


%force dBµV/m
if strcmp(data.LevelUnits,'dBuV/m')
    data.LevelUnits = 'dBµV/m';
end
ylim([actionData.yStart actionData.yStop])
xlim([X(1) X(end)])

xlabel('Time in UTC') % x-axis label
ylabel(data.LevelUnits) % y-axis label

%set fontsizes and the linewidths
set(findobj(gcf, ...
    'type','axes'), ...
    'FontName',plot_fontname, ...
    'FontSize',plot_fontsize, ...
    'FontWeight',plot_fontweight, ...
    'LineWidth', plot_linewidth);

%set white background
set(gcf, 'Color', 'w')

%set the YDir to normal. The measurement start will be displyed at the
%bottom.
set(gca,'YDir','normal')
end