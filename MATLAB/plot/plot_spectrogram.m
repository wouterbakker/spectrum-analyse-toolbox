function figureSpectrogram = plot_spectrogram(data,actionData)
%% Plot parameters
plot_fontsize = 16;
plot_fontname = 'Arial';
plot_fontweight = 'Bold';
plot_linewidth = 2;

%Generate X and Y axis
%kHz,Mhz or Ghz
if data.FreqStop < 10^6
    FrequentyDivider = 10^3; %Hz to kHz
    FrequentyUnit = 'kHz';
elseif data.FreqStop <  10^9
    FrequentyDivider = 10^6; %Hz to MHz
    FrequentyUnit = 'MHz';
else
    FrequentyDivider = 10^9; %Hz to GHz
    FrequentyUnit = 'GHz';
end

switch actionData.statistic
    case "median"
        data.Measurements = movmedian(data.Measurements,actionData.statisticTime,1);
        statsTexts ="Moving median over " + num2str(actionData.statisticTime) + " samples";
    case "min"
        data.Measurements = movmin(data.Measurements,actionData.statisticTime,1);
        statsTexts ="Moving min over " + num2str(actionData.statisticTime) + " samples";
    case "max"
        data.Measurements = movmax(data.Measurements,actionData.statisticTime,1);
        statsTexts ="Moving max over " + num2str(actionData.statisticTime) + " samples";
    case "std"
        data.Measurements = movstd(data.Measurements,actionData.statisticTime,1);
        statsTexts ="Moving std over " + num2str(actionData.statisticTime) + " samples";
    otherwise
        statsTexts ="Raw data";
end

%create X and Y labels
X = linspace( ...
    data.FreqStart/FrequentyDivider, ...
    data.FreqStop/FrequentyDivider, ...
    21);
Y = 1:length(data.MeasurementTimes);

if length(data.Date) > 1
    yticks = round(linspace(1,length(data.MeasurementTimes),15));
    ylabels = cellstr(string(data.MeasurementTimes(yticks),'dd-MMM-yyyy HH:mm'));
else
    yticks = round(linspace(1,length(data.MeasurementTimes),12));
    ylabels = cellstr(timeofday(data.MeasurementTimes(yticks)));  %time labels
end

%generate Alpha data for imagesc
imAlpha=ones(size(data.Measurements));
imAlpha(isnan(data.Measurements))=0;

%create new figure
figureSpectrogram = figure('outerposition',[0 0 1800 1200]);
spectrumPlot = subplot(1,1,1);
imagesc(X,Y,data.Measurements,'AlphaData',imAlpha);

%set the backupground color to gray for missing data
set(gca,'color',0.9*[1 1 1]);

%set labels
set(gca, 'YTick',yticks, 'YTickLabel', ylabels);
if length(data.Date) > 1
    title(["Spectrogram","Date: " + string(data.Date(1)) + ' - ' + string(data.Date(end))+ " Location: " + data.LocationName,statsTexts])
else
    title(["Spectrogram","Date: " + string(data.Date(1)) + " Location: " + data.LocationName,statsTexts])
end

xlabel(['Frequency in ' FrequentyUnit]) % x-axis label
ylabel('Time in UTC') % y-axis label

%set the colormap
colormap (spectrumPlot,jet)
c = colorbar;

%force if the same colorbar if the levelUnit is dBuV/m
if strcmp(data.LevelUnits,'dBuV/m') || strcmp(data.LevelUnits,'dBµV/m')
    c.Ruler.TickLabelFormat =  '%g dBµV/m';
    
end
clim([actionData.yStart actionData.yStop])

%set fontsizes and the linewidths
set(findobj(gcf, ...
    'type','axes'), ...
    'FontName',plot_fontname, ...
    'FontSize',plot_fontsize, ...
    'FontWeight',plot_fontweight, ...
    'LineWidth', plot_linewidth);

%set white background
set(gcf, 'Color', 'w')

%set the YDir to normal. The measurement start will be displyed at the
%bottom. 
set(gca,'YDir','normal')
end