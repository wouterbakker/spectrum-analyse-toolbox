function [output] = convert_ecc_0501_to_mat(filename)
%filename = '/data/ssd/ecc-05-01/2019-02-08-WDMWRK02-526.000-1607.000.txt';

%% open File
% Open the file for reading in text mode.
disp("INFO: Processing: " + filename)
fileID = fopen(filename, 'rt');

%% Read Header

% Read the first line of the file.
textLine = fgetl(fileID);
lineCounter = 1;
while ischar(textLine)
    % Print out what line we're operating on.
    % fprintf('%s\n', textLine);
    % Split the line based on the tab separator
    line = strsplit(textLine,'\t');
    % check length of the line
    if length(line) > 1
        % check if value is a valid nummer
        number = str2double(line(2));
        if isnan(number)
            header.(string(line(1))) = char(line(2));
        else
            header.(string(line(1))) = number;
        end
    end
    % Read the next line.
    textLine = fgetl(fileID);
    if isempty(textLine)
        break;
    end

    lineCounter = lineCounter + 1;
end
%Convert date to matlab datetime
header.Date = datetime(header.Date);

%We like to use SI (derived) units. So covert the Hz
header.FreqStart = header.FreqStart * 10^3;
header.FreqStop = header.FreqStop * 10^3;
header.FreqStep = (header.FreqStop-header.FreqStart)/(header.DataPoints-1);

%Convert GPS coordinates to decimal coordinates.
header.Latitude = strsplit(header.Latitude,'.');
header.Longitude = strsplit(header.Longitude,'.');
LatSecondsWithSGN = char(header.Latitude(3));
LongSecondsWithSGN = char(header.Longitude(3));
N_Or_S = LatSecondsWithSGN(end);
W_Or_E = LongSecondsWithSGN(end);
if N_Or_S == 'N'
    header.Latitude = str2double(header.Latitude(1)) + str2double(header.Latitude(2))/60 + str2double(LatSecondsWithSGN(1:end-1))/3600;
elseif N_Or_S == 'S'
    header.Latitude = -1 * str2double(header.Latitude(1)) + str2double(header.Latitude(2))/60 + str2double(LatSecondsWithSGN(1:end-1))/3600;
else
    disp("ERROR GPS coordinates not in excepted format")
    return
end

if W_Or_E == 'E'
    header.Longitude = str2double(header.Longitude(1)) + str2double(header.Longitude(2))/60 + str2double(LongSecondsWithSGN(1:end-1))/3600;
elseif W_Or_E == 'W'
    header.Longitude = -1 * str2double(header.Longitude(1)) + str2double(header.Longitude(2))/60 + str2double(LongSecondsWithSGN(1:end-1))/3600;
else
    disp("ERROR GPS coordinates not in excepted format")
end

%% Read measurement data
% Read the next line.
textLine = fgetl(fileID);
measurementNumber = 1;

while ischar(textLine)
    line = strsplit(textLine,',');
    % Create data time array
    MeasurementTimes(measurementNumber,1) = header.Date + timeofday(datetime(line(1))); %#ok<AGROW> % Amount of measurements is unknow at this time
    %check if the amount of datapoints is in the line.
    if length(line)-1 == header.DataPoints
        %Measurements(measurementNumber,:) = str2double(line(2:end)); %#ok<AGROW
        Measurements(measurementNumber,:) = sscanf(sprintf(' %s',line{2:end}),'%f',[1,Inf]);%#ok<AGROW
    else
        Measurements(measurementNumber,:) = nan; %#ok<AGROW>
    end
    % Read the next line.
    measurementNumber = measurementNumber + 1;
    textLine = fgetl(fileID);
end

%% All done reading all lines, so close the file and mout the out variable.
fclose(fileID);
output = header;


%% Fill measurements file with NaN, if there is no sample at an expected timestamp.
%Get the total measurement time
output.TotalMeasurementTime = seconds(time(between(MeasurementTimes(1),MeasurementTimes(end))));
[~,~,~,H,MN,S] = datevec(MeasurementTimes);
% Get measurements per hour
[hours,~,index_h] = unique(H);
output.MeasurementsPerHour = zeros(1,24);
output.MeasurementsPerHour(hours+1) = accumarray(index_h,1);
output.HoursWithMeasurement = size(unique(H,'rows'),1);
output.MinutesWithMeasurement = size(unique([H,MN],'rows'),1);
output.SecondsWithMeasurement = size(unique([H,MN,S],'rows'),1);

DurationInSeconds = seconds(time((between(MeasurementTimes(1:end-1),MeasurementTimes(2:end)))));
ExpectedDuration = median(DurationInSeconds);

% Add a 50% margin to include small variations.
ExpectedDurationMax = ExpectedDuration * 1.5;

% Find the missing data
IndexMissingData = find(DurationInSeconds > ExpectedDurationMax);
StartTimestampsMissingData = MeasurementTimes(IndexMissingData);
NumberOfTimestampsMissing = round(DurationInSeconds(IndexMissingData)/ExpectedDuration)-1;

if ~isempty(StartTimestampsMissingData)
    %There is data missing
    TotalNumberOfTimestampsMissing = sum(NumberOfTimestampsMissing);
    disp("WARNING: This measurement file has missing data! " + ...
        "A total of " + num2str(TotalNumberOfTimestampsMissing) + " samples missed.")

    %make the output file until the first missing data
    output.MeasurementTimes = MeasurementTimes(1:IndexMissingData(1));
    output.Measurements = Measurements(1:IndexMissingData(1),:);
    MissedSamplesProcessed = 0;
    for i=1:length(StartTimestampsMissingData)
        for j = 1:NumberOfTimestampsMissing(i)
            output.MeasurementTimes(IndexMissingData(i)+j+MissedSamplesProcessed) = ...
                MeasurementTimes(IndexMissingData(i)) + j * seconds(ExpectedDuration);
            output.Measurements(IndexMissingData(i)+j+MissedSamplesProcessed,:) = nan;
        end
        if i == length(StartTimestampsMissingData)
            NumberOfValidSamples = length(MeasurementTimes)-IndexMissingData(i);
        else
            NumberOfValidSamples = IndexMissingData(i+1)-IndexMissingData(i);
        end
        output.MeasurementTimes(IndexMissingData(i)+j+MissedSamplesProcessed+1:IndexMissingData(i)+j+NumberOfValidSamples+MissedSamplesProcessed)= ...
            MeasurementTimes(IndexMissingData(i)+1:IndexMissingData(i) + NumberOfValidSamples);
        output.Measurements(IndexMissingData(i)+j+MissedSamplesProcessed+1:IndexMissingData(i)+j+NumberOfValidSamples+MissedSamplesProcessed,:)= ...
            Measurements(IndexMissingData(i)+1:IndexMissingData(i) + NumberOfValidSamples,:);
        MissedSamplesProcessed = MissedSamplesProcessed + j;
    end
else
    %Measurement file complete
    output.MeasurementTimes = MeasurementTimes;
    output.Measurements = Measurements;
end
output.MeasurementTimesVec = datevec(output.MeasurementTimes);
end
