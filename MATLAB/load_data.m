function [data]=load_data(filename)
    file = load(filename);
    data = file.data;
end