function action_spectrogram(actionData,jobId)
disp('start spectrogram')
%create random job id and return to client


%query lmt api to get the measurement filenames

measurementFiles = webread('https://lmt-api.spectrum-data.eu/measurementsByDetails', ...
    "Freq",actionData.freqStart,...
    "Location",actionData.location, ...
    "StartDate",actionData.startDate, ...
    "StopDate",actionData.stopDate);

if ~isempty(measurementFiles)
    freqStart = actionData.freqStart;
    freqStop = actionData.freqStop;
    numberOfFiles = length(measurementFiles);
    if numberOfFiles > 1
        MeasurementTimes =[];
        Measurements =[];
        FileType = [];
        Latitude =[];
        Longitude =[];
        FreqStep =[];
        AntennaType =[];
        FilterBandwidth =[];
        LevelUnits=[];
        Date =[];
        DataPoints =[];
        Detector =[];

        parfor i = 1:length(measurementFiles)
            full_filename = measurementFiles(i).FileName;
            dataTemp = load_data(full_filename);
            if size(dataTemp.MeasurementTimes,2)>1
                dataTemp.MeasurementTimes=dataTemp.MeasurementTimes(:);
            end
            MeasurementTimes = [MeasurementTimes ; dataTemp.MeasurementTimes(1:numberOfFiles:end)];
            collumTempStart = (freqStart - dataTemp.FreqStart)/dataTemp.FreqStep + 1;
            collumTempStop = (freqStop - dataTemp.FreqStart)/dataTemp.FreqStep + 1;
            Measurements = [Measurements ; dataTemp.Measurements(1:numberOfFiles:end,collumTempStart:collumTempStop)];
            FileType = [FileType ; dataTemp.FileType];
            Latitude = [Latitude ; dataTemp.Latitude];
            Longitude = [Longitude ; dataTemp.Longitude];
            FreqStep = [FreqStep ; dataTemp.FreqStep];
            AntennaType = [AntennaType ; dataTemp.AntennaType];
            FilterBandwidth = [FilterBandwidth ; dataTemp.FilterBandwidth];
            LevelUnits= [LevelUnits; dataTemp.LevelUnits];
            Date = [Date ; dataTemp.Date];
            DataPoints = [DataPoints ; dataTemp.DataPoints];
            Detector = [Detector ; dataTemp.Detector];
        end
        data.FileType = FileType(1,:);
        data.LocationName = actionData.location;
        data.Latitude = Latitude(1);
        data.Longitude = Longitude(1);
        data.FreqStart = freqStart;
        data.FreqStop = freqStop;
        data.FreqStep = FreqStep(1);
        data.AntennaType = AntennaType(1,:);
        data.FilterBandwidth = FilterBandwidth(1,:);
        data.LevelUnits= LevelUnits(1,:);
        data.Date = Date;
        data.DataPoints = DataPoints(1);
        data.Detector = Detector(1,:);

        %check for missing days
        dateWithoutTime = datevec(data.Date);
        dateWithoutTime = dateWithoutTime(:,1:3);
        dateWithoutTime = datetime(dateWithoutTime);
        alldays = datetime(actionData.startDate) : datetime(actionData.stopDate);
        index = ismember(alldays',dateWithoutTime,'rows');
        missingDates = alldays(index==0);
        if ~(isempty(missingDates))
            %create MeasurementTime array
            for d = missingDates
                dayarray = d:minutes(1):d+minutes(1439);
                MeasurementTimesToAdd = dayarray(1:numberOfFiles:end);
                nanToAdd = nan(length(MeasurementTimesToAdd),size(Measurements,2));
                MeasurementTimes = [MeasurementTimes ; MeasurementTimesToAdd'];
                Measurements = [Measurements ; nanToAdd];

            end
        end
        %sort by timestamps
        [data.MeasurementTimes,index] = sort(MeasurementTimes);
        data.Measurements = Measurements(index,:);
        data.Date = sort(data.Date);

    else
        full_filename = measurementFiles.FileName;
        data = load_data(full_filename);
        collumTempStart = (freqStart - data.FreqStart)/data.FreqStep + 1;
        collumTempStop = (freqStop - data.FreqStart)/data.FreqStep + 1;
        AllMeasurements = data.Measurements;
        clear data.Measurements
        data.FreqStart = freqStart;
        data.FreqStop = freqStop;
        data.Measurements = AllMeasurements(:,collumTempStart:collumTempStop);
    end
    plot_spectrogram(data,actionData);
    export_filename = ['/data/ssd/dynamicplots/' jobId '.png'];
    export_fig(export_filename);
else
    error('No measurements found')
end
end

