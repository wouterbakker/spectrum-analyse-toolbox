%function matlab_server_worker()
load ('lmt_api_config.mat','config_lmt_api')

%Job done, send result back.
uri = config_lmt_api.uri;
options = weboptions(...
    'MediaType', 'text/plain; charset=utf-8',...
    'ContentType', 'json',...
    'Timeout', 30, ...
    'HeaderFields', {'Authorization' ['Bearer ' config_lmt_api.token]}...
    );

while true
    %check for new jobs
    try
        actionData = webread('https://lmt-api.spectrum-data.eu/job');
        if isfield(actionData,'ID')
            disp(actionData.ID)

            try
                if strcmp(actionData.action,"ytplot")
                    action_yt_plot(actionData,num2str(actionData.ID))
                elseif strcmp(actionData.action,"spectrogram")
                    action_spectrogram(actionData,num2str(actionData.ID))
                end
                result.id = actionData.ID;
                result.resultUrl = "https://img.spectrum-data.eu/" + num2str(actionData.ID) + ".png";
                result.resultDataUrl = "https://img.spectrum-data.eu/" + num2str(actionData.ID) + ".json";

                body = jsonencode(result);
                jobDoneUrl = 'https://lmt-api.spectrum-data.eu/jobDone';
                webwrite(jobDoneUrl, body, options);
            catch e
                disp('job failed')

                try
                    result.id = actionData.ID;
                    result.resultUrl = "failed";
                    result.resultDataUrl = "failed";
                    body = jsonencode(result);
                    jobDoneUrl = 'https://lmt-api.spectrum-data.eu/jobDone';
                    webwrite(jobDoneUrl, body, options);
                catch e
                    fprintf(1,'The identifier was:\n%s',e.identifier);
                    fprintf(1,'There was an error! The message was:\n%s',e.message);
                end
                fprintf(1,'The identifier was:\n%s',e.identifier);
                fprintf(1,'There was an error! The message was:\n%s',e.message);
            end

        end
    catch e
        disp('connection to api failed wait 5 secondes')
        fprintf(1,'The identifier was:\n%s',e.identifier);
        fprintf(1,'There was an error! The message was:\n%s',e.message);
        pause(5)

    end
    pause(1)
end
%end
