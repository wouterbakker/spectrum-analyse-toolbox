function action_yt_plot(actionData,jobId)
disp('start ytplot')
%jobId = 0
%create random job id and return to client


%query lmt api to get the measurement filenames
measurementFiles = webread('https://lmt-api.spectrum-data.eu/measurementsByDetails', ...
    "Freq",actionData.freqStart,...
    "Location",actionData.location, ...
    "StartDate",actionData.startDate, ...
    "StopDate",actionData.stopDate);
disp(actionData)
if ~isempty(measurementFiles)
    yt_freq = actionData.freqStart;
    numberOfFiles = length(measurementFiles);
    if numberOfFiles > 1
        MeasurementTimes =[];
        Measurements =[];
        FileType = [];
        Latitude =[];
        Longitude =[];
        FreqStart =[];
        FreqStop =[];
        FreqStep =[];
        AntennaType =[];
        FilterBandwidth =[];
        LevelUnits=[];
        Date =[];
        DataPoints =[];
        Detector =[];

        parfor i = 1:length(measurementFiles)
            full_filename = measurementFiles(i).FileName;
            dataTemp = load_data(full_filename);
            if size(dataTemp.MeasurementTimes,2)>1
                dataTemp.MeasurementTimes=dataTemp.MeasurementTimes(:);
            end
            collumTemp = round((yt_freq - dataTemp.FreqStart)/dataTemp.FreqStep + 1);
            MeasurementTimes = [MeasurementTimes ; dataTemp.MeasurementTimes(1:numberOfFiles:end)];
            Measurements = [Measurements ; dataTemp.Measurements(1:numberOfFiles:end,collumTemp)];
            FileType = [FileType ; dataTemp.FileType];
            Latitude = [Latitude ; dataTemp.Latitude];
            Longitude = [Longitude ; dataTemp.Longitude];
            FreqStart = [FreqStart ; dataTemp.FreqStart];
            FreqStop = [FreqStop ; dataTemp.FreqStop];
            FreqStep = [FreqStep ; dataTemp.FreqStep];
            AntennaType = [AntennaType ; dataTemp.AntennaType];
            FilterBandwidth = [FilterBandwidth ; dataTemp.FilterBandwidth];
            LevelUnits= [LevelUnits; dataTemp.LevelUnits];
            Date = [Date ; dataTemp.Date];
            DataPoints = [DataPoints ; dataTemp.DataPoints];
            Detector = [Detector ; dataTemp.Detector];
        end
        data.FileType = FileType(1,:);
        data.LocationName = actionData.location;
        data.Latitude = Latitude(1);
        data.Longitude = Longitude(1);
        data.FreqStart = FreqStart(1);
        data.FreqStop = FreqStop(1);
        data.FreqStep = FreqStep(1);
        data.AntennaType = AntennaType(1,:);
        data.FilterBandwidth = FilterBandwidth(1,:);
        data.LevelUnits= LevelUnits(1,:);
        data.Date = Date;
        data.DataPoints = DataPoints(1);
        data.Detector = Detector(1,:);
        data.MeasurementTimes = MeasurementTimes;
        data.Measurements = Measurements;
        %check for missing days
        dateWithoutTime = datevec(data.Date);
        dateWithoutTime = dateWithoutTime(:,1:3);
        dateWithoutTime = datetime(dateWithoutTime);
        alldays = datetime(actionData.startDate) : datetime(actionData.stopDate);
        index = ismember(alldays',dateWithoutTime,'rows');
        missingDates = alldays(index==0);
        if ~(isempty(missingDates))
            %create MeasurementTime array
            for d = missingDates
                dayarray = d:minutes(1):d+minutes(1439);
                MeasurementTimesToAdd = dayarray(1:numberOfFiles:end);
                nanToAdd = nan(length(MeasurementTimesToAdd),size(Measurements,2));
                MeasurementTimes = [MeasurementTimes ; MeasurementTimesToAdd'];
                Measurements = [Measurements ; nanToAdd];
            end
        end
        %sort by timestamps
        [data.MeasurementTimes,index] = sort(MeasurementTimes);
        data.Measurements = Measurements(index,:);
        data.Date = sort(data.Date);
    else
        full_filename = measurementFiles.FileName;
        data = load_data(full_filename);
        collum = round((yt_freq - data.FreqStart)/data.FreqStep + 1);
        data.Measurements = data.Measurements(:,collum);
    end
    %Check start and stop times
    startdateWithTime = datetime(actionData.startDate) + timeofday(datetime(actionData.startTime,'InputFormat','HH:mm'));
    stopdateWithTime = datetime(actionData.stopDate) + timeofday(datetime(actionData.stopTime,'InputFormat','HH:mm'));
    % Compute the minimum difference between reference datetime and all datetime points
    diffsStartTime = abs(data.MeasurementTimes - startdateWithTime);
    diffsStopTime = abs(data.MeasurementTimes - stopdateWithTime);
    [~, idxStartTime] = min(diffsStartTime);
    [~, idxStopTime] = min(diffsStopTime);
    data.MeasurementTimes = data.MeasurementTimes(idxStartTime:idxStopTime);
    data.Measurements = data.Measurements(idxStartTime:idxStopTime);

else
    error('No measurements found')
end
%export generated image
%plot_yt(data,actionData);
%export_filename_img = ['/data/ssd/dynamicplots/' jobId '.png'];
%export_fig(export_filename_img);
%check if a statistic is required
switch actionData.statistic
    case "median"
        data.Measurements = movmedian(data.Measurements,actionData.statisticTime);
    case "min"
        data.Measurements = movmin(data.Measurements,actionData.statisticTime);
    case "max"
        data.Measurements = movmax(data.Measurements,actionData.statisticTime);
    case "std"
        data.Measurements = movstd(data.Measurements,actionData.statisticTime);
    otherwise
end
export_filename_json = ['/data/ssd/dynamicplots/' jobId '.json'];
%export data as json
NumberOfMeasurements = size(data.Measurements,1);
if  NumberOfMeasurements > 250
    devider = round(NumberOfMeasurements/250);
    dataExport.Measurements = data.Measurements(1:devider:end);
    dataExport.MeasurementTimes = data.MeasurementTimes(1:devider:end);
else
    dataExport.Measurements = data.Measurements;
    dataExport.MeasurementTimes = data.MeasurementTimes;
end

jsonExport = jsonencode(dataExport);

fid = fopen(export_filename_json,'w');
fprintf(fid,'%s',jsonExport);
fclose(fid);
end